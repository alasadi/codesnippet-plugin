import sublime
import sublime_plugin
import requests
import urllib
import json

class CodesnippetCommand(sublime_plugin.TextCommand):
	def run(self, edit):
		global settings
		settings = sublime.load_settings('Preferences.sublime-settings')
		key_source = settings.get('codesnippet_key')

		selection = [region for region in self.view.sel() if not region.empty()]
		snippet = [self.view.substr(region) for region in selection]
		
		url = "http://codesnippet.at/api"
		token = key_source[0]

		params = {
				'api_token': token,
				'snippet_name': "Uploaded Snippet from Sublime Plugin",
				'tags': "",
				'snippet': str(snippet[0]),
				'snippet_status': 1,
				'plugin_upload': 1
				}
		if requests.get(url + "/snippets").status_code == requests.codes.ok:
			r = requests.post(url, data = params)
			sublime.message_dialog("Snippet uploaded ")
		else:
			print(requests.get(url).status_code)

		


